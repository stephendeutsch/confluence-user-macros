# Confluence User Macros #

These are a couple of user macros that I have created for Confluence in order to add new features or make administration easier.

They have not been thoroughly tested, but they should work at least on all versions from 5.0 - 5.4.4.

If you find any issues, please [let me know](mailto:stephen.deutsch@zanox.com).

I am making these available for free under the [Apache license](http://www.apache.org/licenses/LICENSE-2.0.html), but if you find any of them useful, consider [making a donation via Paypal.](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=8Z6CHCT4XCLPA&lc=US&item_name=Stephen%20Deutsch&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)