## Macro title: Redirect Macro
## Body processing: N
##
## Developed by: Stephen Deutsch
##
## This macro redirects the user from the current page to a new page defined in the options.
## This macro more or less duplicates the functionality of the ServiceRocket Redirection Plugin

## Copyright 2014 zanox AG
## 
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
## 
##     http://www.apache.org/licenses/LICENSE-2.0
## 
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.

## @param location:title=Location|type=string|required=true|desc=The URL to redirect to
## @param delay:title=Delay|type=string|default=0|desc=The number of seconds to delay before redirecting the browser.

#set ( $Integer = 0 )
#set ( $delay = "0" )
#set ( $delay = $paramdelay )
#set ( $delay = $Integer.parseInt($delay) * 1000 )
#if ( $req.getParameter('redirect') != "false" )
  <script>
  setTimeout(function() {
    window.location = "$paramlocation";
  }, $delay);
  </script>
#end

<ac:macro ac:name="note">
  <ac:parameter ac:name="title">Redirection notice</ac:parameter>
  <ac:rich-text-body>
    <p>This page will redirect to <a href="$paramlocation">$paramlocation</a> in about $delay seconds.</p>
  </ac:rich-text-body>
</ac:macro>